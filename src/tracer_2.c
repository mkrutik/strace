#include "ft_strace.h"

static int is_termination_signal(int sig)
{
    int res = 0;

    switch (sig)
    {
        case SIGABRT:
        case SIGBUS :
        case SIGFPE :
        case SIGILL :
        case SIGQUIT :
        case SIGSEGV :
        case SIGTRAP :
        case SIGXCPU :
        case SIGXFSZ :
        case SIGSYS :
        case SIGSTKFLT :
        case SIGALRM :
        case SIGHUP :
        case SIGINT :
        case SIGIO :
        case SIGKILL :
        case SIGPIPE :
        case SIGPROF :
        case SIGPWR :
        case SIGTERM :
        case SIGUSR1 :
        case SIGUSR2 :
        case SIGVTALRM :
            res = 1;
            break;

        default:
            res = 0;
            break;
    }

    return res;
}

int  wait_for_syscall(void)
{
    // Wait for `good` syscalls and exit status
    while (1)
    {
        // Restore process and stop on state chaged
        if (-1 == ptrace(PTRACE_SYSCALL, g_child, 0, 0))
            return 1;

        // Get process status
        waitpid(g_child, &g_status, 0);

        if (WIFSTOPPED(g_status))
        {
            if (WSTOPSIG(g_status) & 0x80)
                return 0;

            if (WSTOPSIG(g_status) != SIGTRAP)
            {
                fprintf(stderr, "%s--- %s ---\n", COLOR_GREEN, get_signal_name(WSTOPSIG(g_status)));
                return (1 == is_termination_signal(WSTOPSIG(g_status))) ? 1 : -5;
            }
        }

        // process exit
        if (WIFEXITED(g_status))
            return 1;
    }
    return 0;
}

static void handle_sigint(int sig)
{
    (void) sig;

    kill(g_child, SIGKILL);
    fprintf(stderr, COLOR_RESET);
    exit(-1);
}

int attache_to_child(void)
{
    if (-1 == ptrace(PTRACE_SEIZE, g_child, NULL, NULL))
        return (-1);

    waitpid(g_child, &g_status, 0);
    if (-1 == ptrace(PTRACE_SETOPTIONS, g_child, 0, PTRACE_O_TRACESYSGOOD | PTRACE_O_EXITKILL))
        return (-1);

    signal(SIGINT, handle_sigint);
    return (0);
}