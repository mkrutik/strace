#include "ft_strace.h"

static void    find_file_in_path(char file_path[FILENAME_MAX], const char *name)
{
    struct stat fstat;
    char        *env;
    char        *token;
    int         len;

    env = getenv("PATH");
    if (NULL == env)
        return ;

    while (1)
    {
        token = strtok(env, ":");
        if (token == NULL)
        {
            file_path[0] = '\0';
            break;
        }
        env = NULL;

        len = strlen(token);
        if (len + strlen(name) > FILENAME_MAX)
            continue ;

        strcpy(file_path, token);
        if (file_path[len -1] != '/')
        {
            file_path[len] = '/';
            len++;
        }
        if ((len + strlen(name)) > FILENAME_MAX)
            continue ;

        strcpy(file_path + len, name);
        if (-1 != stat(file_path, &fstat) &&
                S_ISREG(fstat.st_mode) &&
                (fstat.st_mode & (S_IXGRP | S_IXUSR | S_IXOTH)))
            break;
    }
}

int    check_file(char file_path[FILENAME_MAX], const char *name)
{
    struct stat fstat;

    if (strlen(name) > FILENAME_MAX)
    {
        fprintf(stderr, "%sError: File name too long!\n%s", COLOR_RED, COLOR_RESET);
        return (-1);
    }

    if (strchr(name,'/'))
        strcpy(file_path, name);
    else
        find_file_in_path(file_path, name);

    if (-1 == stat(file_path, &fstat))
    {
        fprintf(stderr, "%sError: Can't stat '%s': No such file or directory\n%s", COLOR_RED, name, COLOR_RESET);
        return (-1);
    }

    return (0);
}