#include "ft_strace.h"

static int     block_signals(void)
{
    if (SIG_ERR == signal(SIGINT, SIG_IGN) ||
        SIG_ERR == signal(SIGTERM, SIG_IGN) ||
        SIG_ERR == signal(SIGHUP, SIG_IGN) ||
        SIG_ERR == signal(SIGQUIT, SIG_IGN) ||
        SIG_ERR == signal(SIGPIPE, SIG_IGN) ||
        SIG_ERR == signal(SIGTTOU, SIG_IGN) ||
        SIG_ERR == signal(SIGTTIN, SIG_IGN))
        return (-1);

    return (0);
}

static int get_registers(t_iovec *iov)
{
    iov->iov_len = sizeof(t_reg_inf);
    memset(iov->iov_base, 0, sizeof(t_reg_inf));

    if (-1 == ptrace(PTRACE_GETREGSET, g_child, NT_PRSTATUS, iov))
        return 1;

    iov->is_x64 = true;
    if (iov->iov_len == sizeof(t_x32_user_regs_struct))
        iov->is_x64 = false;

    return 0;
}

static int     retrive_info(t_iovec *iov)
{
    int res = 0;
    // Restore tracee process and wait for a new syscall
    if (1 == (res = wait_for_syscall()))
        return -1;

    if (-5 == res)
        return -5;

    if (1 == get_registers(iov))
        return -1;

    if (iov->is_x64)
    {
        iov->sys_inf = get_syscall_64( ((t_reg_inf*)iov->iov_base)->reg_64.orig_rax);
        iov->print_len = fprintf(stderr, "%s%s%s(",COLOR_YELLOW, ((iov->sys_inf != NULL) ? iov->sys_inf->name : "unknown"), COLOR_BLUE);
        iov->print_len += print_x64_args(iov->sys_inf, &((t_reg_inf*)iov->iov_base)->reg_64);
    }
    else
    {
        iov->sys_inf = get_syscall_32( ((t_reg_inf*)iov->iov_base)->reg_32.orig_eax);
        iov->print_len = fprintf(stderr, "%s%s%s(",COLOR_YELLOW, ((iov->sys_inf != NULL) ? iov->sys_inf->name : "unknown"), COLOR_BLUE);
        iov->print_len += print_x32_args(iov->sys_inf, &((t_reg_inf*)iov->iov_base)->reg_32);
    }

    iov->print_len -= (strlen(COLOR_YELLOW) + strlen(COLOR_BLUE));

    if (0 == strcmp("exit_group", iov->sys_inf->name))
        fprintf(stderr, ")%s%*c = ?\n",COLOR_BLUE, (39 - ((int) ++(iov->print_len)) ), ' ');

    return iov->print_len;
}

static int     retrive_result(t_iovec *iov)
{
    // Restore tracee process and wait for syscall result
    if (0 != wait_for_syscall() ||
        -1 == get_registers(iov))
        return -1;

    if (iov->is_x64)
        print_ret( ((t_reg_inf*)iov->iov_base)->reg_64.rax, iov->sys_inf->ret, iov->print_len, iov->is_x64);
    else
        print_ret( (unsigned long long)(((t_reg_inf*)iov->iov_base)->reg_32.eax), iov->sys_inf->ret, iov->print_len, iov->is_x64);

    return 0;
}

void    tracer(void)
{
    t_reg_inf regs;
    t_iovec   iov;

    if (-1 == block_signals() ||
        -1 == attache_to_child())
        return ;

    iov.iov_base = &regs;

    while (1)
    {
        int res = res = retrive_info(&iov);

        if (-1 == res)
            break;
        else if (-5 == res)
            continue;

        if (-1 == retrive_result(&iov))
            break;

        fprintf(stderr, COLOR_RESET);
    }
}