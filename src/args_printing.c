#include "ft_strace.h"

static size_t print_string(const char *buff, size_t len)
{
    size_t res_len = 0;

    res_len += fprintf(stderr, "\"");

    for (size_t i = 0; i < len; ++i)
    {
        if (iscntrl(buff[i]) &&
            buff[i] >= '\t' && buff[i] <= '\r')
        {
            switch (buff[i])
            {
                case '\t':
                    fprintf(stderr, "\\t");
                    break;
                case '\n':
                    fprintf(stderr, "\\n");
                    break;
                case '\v':
                    fprintf(stderr, "\\v");
                    break;
                case '\f':
                    fprintf(stderr, "\\f");
                    break;
                case '\r':
                    fprintf(stderr, "\\r");
                    break;
            }
            res_len += 3;
        }
        else if (isprint(buff[i]))
            res_len += fprintf(stderr, "%c", buff[i]);
        else
            res_len += fprintf(stderr,"\\%o",buff[i]);
    }

    res_len += fprintf(stderr, "\"");

    return res_len;
}

static int  print_arg(const unsigned long long reg, const t_arg_type type, const unsigned long long write_len)
{
    size_t res_len = 0;

    switch (type)
    {
        case ARG_STR:
        {
            char buff[50] = {};

            read_string(buff, reg);
            const size_t buff_len = strlen(buff);

            if (32 < buff_len || 32 < write_len)
            {
                res_len += print_string(buff, 32);
                res_len += fprintf(stderr, "...");
            }
            else if (buff_len == 0)
                res_len += print_string(buff, (int)write_len);
            else
                res_len += print_string(buff, buff_len);

            break;
        }
        case ARG_LL_UINT:
            res_len = fprintf(stderr, "%llu", reg);
            break;

        case ARG_L_UINT:
            res_len = fprintf(stderr, "%lu", (unsigned long)reg);
            break;

        case ARG_UINT:
            res_len = fprintf(stderr, "%u", (unsigned)reg);
            break;

        case ARG_LL_INT:
            res_len = fprintf(stderr, "%lli", (long long)reg);
            break;

        case ARG_L_INT:
            res_len = fprintf(stderr, "%li", (long)reg);
            break;

        case ARG_ADDR:
            if (0 == reg)
                res_len = fprintf(stderr, "NULL");
            else
                res_len = fprintf(stderr, "%#llx", reg);
            break;

        case ARG_INT:
        default:
            res_len = fprintf(stderr, "%i", (int)reg);
            break;
    }

    return res_len;
}

void    print_ret(const unsigned long long reg, t_arg_type type, size_t print_len, bool is_x64)
{
    print_len += fprintf(stderr, ")%s", COLOR_GREEN);
    print_len -= strlen(COLOR_GREEN);

    if (39 > print_len)
        fprintf(stderr, "%*c", (39 - (int) print_len), ' ');

    fprintf(stderr, " = ");

    if (!is_x64 && 0 > ((int)reg) && get_errno_inf(-((int)reg), ERRN_NAME))
        fprintf(stderr, "%s-1 %s (%s)", COLOR_RED, get_errno_inf(-((int)reg), ERRN_NAME),
                                      get_errno_inf(-((int)reg), ERRN_DESC));
    else if (is_x64 && 0 > ((long)reg) && get_errno_inf(-((long)reg), ERRN_NAME))
        fprintf(stderr, "%s-1 %s (%s)", COLOR_RED, get_errno_inf(-((long)reg), ERRN_NAME),
                                      get_errno_inf(-((long)reg), ERRN_DESC));
    else
        (void) print_arg(reg, type, 0 /* not used*/);

    fprintf(stderr, "\n");
}

size_t  print_x64_args(const t_syscall_inf *inf,  struct user_regs_struct *regs)
{
    size_t len = 0;
    const unsigned long long args[6] = {regs->rdi, regs->rsi, regs->rdx, regs->r10, regs->r8, regs->r9};

    for (uint8_t i = 0; i < inf->n_args; ++i)
    {

        len += print_arg(args[i], inf->args[i], (!strcmp("write", inf->name) ? args[i + 1] : 0));

        if (i + 1 < inf->n_args)
            len += fprintf(stderr, ", ");
    }

    return len;
}

size_t  print_x32_args(const t_syscall_inf *inf, const t_x32_user_regs_struct *regs)
{
    size_t len = 0;
    const unsigned long long args[6] = {regs->ebx, regs->ecx, regs->edx, regs->esi, regs->edi, regs->ebp};

    for (uint8_t i = 0; i < inf->n_args; ++i)
    {
        len += print_arg(args[i], inf->args[i], (!strcmp("write", inf->name) ? args[i + 1] : 0));

        if (i + 1 < inf->n_args)
            len += fprintf(stderr, ", ");
    }

    return len;
}