#include "ft_strace.h"

pid_t   g_child;
int     g_status;

void print_result(int argc, char *argv[])
{
    if (WIFEXITED(g_status))
		fprintf(stderr, "%s+++ exited with %d +++", COLOR_GREEN, WEXITSTATUS(g_status));
	else if (WIFSTOPPED(g_status))
	{
		fprintf(stderr,"%s+++ killed by %s +++\n", COLOR_RED, get_signal_name(WSTOPSIG(g_status)));
		fprintf(stderr,"[1]    %d %s    ", g_child, strsignal(WSTOPSIG(g_status)));

        for (int i = 0; i < argc; ++i)
            fprintf(stderr,"%s ", argv[i]);

	}

    fprintf(stderr, "\n%s", COLOR_RESET);
}

int main(int argc, char *argv[])
{
    char        file_path[FILENAME_MAX];
    bool        c_flag;
    int         next_arg_index;

    next_arg_index = get_arguments(argc, argv, &c_flag);
    if (1 == argc || 0 == (argc - next_arg_index))
    {
        fprintf(stderr, "%sUsage: %s PROG [ARGS]\n%s", COLOR_RED, argv[0], COLOR_RESET);
        return (-1);
    }

    if (-1 != check_file(file_path, argv[next_arg_index]))
    {
        g_child = fork();
        if (0 == g_child)
            return tracee(file_path, (argc - next_arg_index), (argv + next_arg_index));
        else if (0 < g_child)
        {
            tracer();
            print_result(argc, argv);
        }
        else
            fprintf(stderr, "%sError: %s\n%s", COLOR_RED, strerror(errno), COLOR_RESET);
    }

    return (-1);
}