#include "ft_arguments.h"

int get_arguments(int argc, char **argv, bool *arguments)
{
    char c;

    *arguments = false;
    opterr = 0;
    while (-1 != (c = getopt(argc, argv, "+c")))
    {
        switch (c)
        {
            case 'c':
                *arguments = true;
                break;

            default:
                break;
        }
    }

    return optind;
}