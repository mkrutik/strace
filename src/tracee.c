#include "ft_strace.h"

int    tracee(const char* file_path, int argc, char *argv[])
{
    char    *new_args[argc + 1];

    memcpy(new_args, argv, argc * sizeof(char*));
    new_args[argc] = NULL;

    kill(getpid(), SIGSTOP);
    return execvp(file_path, new_args);
}