#include "ft_strace.h"

void    read_string(char buff[50], const unsigned long long int addr)
{
    int read = 0;
    unsigned long tmp;

    while (read < 50 /* By default it print just 32 symbols*/)
    {
        tmp = ptrace(PTRACE_PEEKDATA, g_child, addr + read);
        if(errno != 0) {
            buff[read] = '\0';
            break;
        }

        memcpy(buff + read, &tmp, sizeof(tmp));
        read += sizeof(tmp);
    }
}