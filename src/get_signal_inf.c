#include "ft_signal.h"

const char *get_signal_name(int sig_num)
{
    static const t_signal signals[] = {
        { .num = SIGHUP,    .name = "SIGHUP"},
        { .num = SIGINT,    .name = "SIGINT"},
        { .num = SIGQUIT,   .name = "SIGQUIT"},
        { .num = SIGILL,    .name = "SIGILL"},
        { .num = SIGTRAP,   .name = "SIGTRAP"},
        { .num = SIGABRT,   .name = "SIGABRT"},
        { .num = SIGBUS,    .name = "SIGBUS"},
        { .num = SIGFPE,    .name = "SIGFPE"},
        { .num = SIGKILL,   .name = "SIGKILL"},
        { .num = SIGUSR1,   .name = "SIGUSR1"},
        { .num = SIGSEGV,   .name = "SIGSEGV"},
        { .num = SIGUSR2,   .name = "SIGUSR2"},
        { .num = SIGPIPE,   .name = "SIGPIPE"},
        { .num = SIGALRM,   .name = "SIGALRM"},
        { .num = SIGTERM,   .name = "SIGTERM"},
        { .num = SIGSTKFLT, .name = "SIGSTKFLT"},
        { .num = SIGCHLD,   .name = "SIGCHLD"},
        { .num = SIGCONT,   .name = "SIGCONT"},
        { .num = SIGSTOP,   .name = "SIGSTOP"},
        { .num = SIGTSTP,   .name = "SIGTSTP"},
        { .num = SIGTTIN,   .name = "SIGTTIN"},
        { .num = SIGTTOU,   .name = "SIGTTOU"},
        { .num = SIGURG,    .name = "SIGURG"},
        { .num = SIGXCPU,   .name = "SIGXCPU"},
        { .num = SIGXFSZ,   .name = "SIGXFSZ"},
        { .num = SIGVTALRM, .name = "SIGVTALRM"},
        { .num = SIGPROF,   .name = "SIGPROF"},
        { .num = SIGWINCH,  .name = "SIGWINCH"},
        { .num = SIGPOLL,   .name = "SIGPOLL"},
        { .num = SIGPWR,    .name = "SIGPWR"},
        { .num = SIGSYS,    .name = "SIGSYS"}
    };
    static const int LIST_SIZE = sizeof(signals) / sizeof(t_signal);

    const char *res = NULL;
    for (int i = 0; i < LIST_SIZE; ++i)
    {
        if (signals[i].num == sig_num)
        {
            res = signals[i].name;
            break;
        }
    }
    return res;
}