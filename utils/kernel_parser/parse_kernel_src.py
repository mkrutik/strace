# Python 2.7.16
import sys
import re
import os
import subprocess

def process_define(syscalls, text):
    (name, types) = None, None

    if text.startswith('SYSCALL_DEFINE('):
        m = re.search(r'^SYSCALL_DEFINE\(([^)]+)\)\(([^)]+)\)$', text)
        if not m:
            print("Unable to parse:", text)
            return

        _, name, args = m.groups()
        types = [s.strip().rsplit(" ", 1)[0] for s in args.split(",")]

    else:
        m = re.search(r'^SYSCALL_DEFINE\d\(([^,]+)\s*(?:,\s*([^)]+))?\)$', text)
        if not m:
            print("Unable to parse:", text)
            return

        name, argstr = m.groups()
        if argstr is not None:
            argspec = [s.strip() for s in argstr.split(",")]
            types = []
            index = 0

            while (index + 1) in range(len(argspec)):
                types.append('{} {}'.format(argspec[index], argspec[index + 1]))
                index += 2

            while len(types) < 6:
                types.append("")

        else:
            types = ["","","","","",""]

    syscalls[name] = types


def find_args(linux):
    syscalls = {}
    # find = os.popen('find %s -name "*.c" -print' % (linux)).read()
    find = subprocess.Popen(["find"] +
                             [os.path.join(linux, d) for d in
                              "arch/x86 fs include ipc kernel mm net security".split()] +
                            ["-name", "*.c", "-print"],
                            stdout = subprocess.PIPE)
    for f in find.stdout:
    # for f in find.splitlines():
        fh = open(f.strip())
        in_syscall = False
        text = ''

        for line in fh:
            line = line.strip()

            if not in_syscall and 'SYSCALL_DEFINE' in line:
                text = ''
                in_syscall = True

            if in_syscall:
                text += line

                if line.endswith(')'):
                    in_syscall = False
                    process_define(syscalls, text)
                else:
                    text += " "
    return syscalls


def create_csv_file(syscalls):
    out = open('all_syscalls.csv', 'w')
    out.write('"name","argc","first","second","third","fourth","fifth","sixth"\n')

    for elem in sorted(syscalls):

        args = 0
        for i in syscalls[elem]:
            if i != '':
                args +=1

        out.write('"%s","%d",' % (elem, args))
        for i in syscalls[elem]:
            out.write('"%s",' % i)

        out.write("\n")
    out.close()


def main(args):
    if not args:
        print("Usage: %s [path to kernel dir]" % (sys.argv[0],), file=sys.stderr)
        return 1
    linux_dir = args[0]

    syscalls_set = find_args(linux_dir)
    create_csv_file(syscalls_set)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))