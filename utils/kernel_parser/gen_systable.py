# Python 2.7.16
import csv
import sys
import re
import os
import platform

def parse_type(ctype):
    if  re.search(r'((const)?\s*(char){1}\s*[*]{1})\w*$', ctype) or \
        re.search(r'((const)?\s*(char){1}\s*[*]{1}\s*(const){1}\s*)\w*$', ctype) or \
        re.search(r'((const)?\s*(char){1}\s*(const)?\s*[*]{1}[\s\t]*)\w*$', ctype):

        return "ARG_STR"

    if  re.search(r'[*]+', ctype) or \
        re.search(r'(struct){1}', ctype) or \
        re.search(r'(timer_t){1}', ctype) or \
        re.search(r'(cap_user_data_t){1}', ctype) or \
        re.search(r'(cap_user_header_t){1}', ctype) or \
        re.search(r'(__sighandler_t){1}', ctype) or \
        re.search(r'(union){1}', ctype) :

        return "ARG_ADDR"

    if  re.search(r'(unsigned){1}', ctype) or \
        re.search(r'(u32){1}', ctype) or \
        re.search(r'(u64){1}', ctype) or \
        re.search(r'(uid_t){1}', ctype) or \
        re.search(r'(gid_t){1}', ctype) or \
        re.search(r'(mode_t){1}', ctype) or \
        re.search(r'(aio_context_t){1}', ctype) or \
        re.search(r'(umode_t){1}', ctype) or \
        re.search(r'(qid_t){1}', ctype) or \
        re.search(r'(old_sigset_t){1}', ctype) or \
        re.search(r'(uint){1}', ctype) :

        return "ARG_UINT"

# int
# __s32
# pid_t
# long
# size_t
# clockid_t
# off_t
# loff_t
# key_t
# mqd_t
# key_serial_t
    return "ARG_INT"

def write_file(src_file, arch, syscall_types, syscall_names):
        out = open(src_file, 'w')

        print('#include "ft_syscall.h"\n', file=out)
        print('const t_syscall_inf* get_syscall_%s(int num)' % (arch), file=out)
        print('{', file=out)
        print('\tstatic t_syscall_inf syscalls[] = {', file=out)

        unique_arg = set()

        for elem in syscall_names:
            name = elem['name']
            num = elem['num']
            syscall_inf = {}

            name_in_list = False
            for index in sorted(syscall_types):
                if name == syscall_types[index]['name']:
                    syscall_inf = syscall_types[index]
                    name_in_list = True
                    break

            if not name_in_list:
                print('// Unimplemented system call. "%s"' % (name), file=out)
                continue

            args = syscall_inf['args']
            argv = syscall_inf['argv']
            ret_type = get_ret_arg(name)

            if ret_type != None:
                ret_type = parse_type(ret_type)
            else:
                ret_type = 'ARG_INT /* None */'

            out.write("\t\t[%+3s]  { %-26s %-10s %d, " % (num, "\"" + name + "\",", ret_type + ",", args,))

            if args > 0:
                out.write('{')
                for arg in argv:
                    if arg != None and arg != '' and arg != 'NOT IMPLEMENTED':
                        out.write("%s, " % (parse_type(arg)))
                        arg = re.sub(r'(\s?[\w]+)$', '', arg)

                        if not re.search(r'[*]+', arg):
                            unique_arg.add(arg)

                    elif arg == 'NOT IMPLEMENTED':
                        out.write('/* NOT IMPLEMENTED !!! */')

                out.write('}')
            else:
                out.write("{}")
            print('},', file=out)

        print('\t};', file=out)
        print('\tstatic const int LIST_SIZE = sizeof(syscalls) / sizeof(t_syscall_inf);\n', file=out)
        print('\tconst t_syscall_inf *res = NULL;', file=out)
        print('\tif (num >= 0 && num < LIST_SIZE)', file=out)
        print('\t\tres = &(syscalls[num]);', file=out)
        print('\treturn res;', file=out)
        print('}', file=out)
        out.close()

        # write all data types to file for testing purpose
        out = open('args_{}.h'.format(arch), 'w')
        for arg in sorted(unique_arg):
            print(arg, file=out)
        out.close()

def get_cmd_output(cmd):
    cmd_out = ''
    cmd_out = os.popen(cmd).read()
    return cmd_out

def get_file_content(file_name):
    content = ''
    if os.path.exists(file_name):
        fp = open(file_name, "r")
        content = fp.read()
        fp.close()
    return content

# return list of syscall_info objects
def get_syscalls_names(arch):
    names = []

    cmd_output = get_cmd_output('locate -r "/usr/include/.*/asm/unistd_%s.h$"' % (arch))
    if cmd_output == '':
        sys.exit("Can't find any unistd_64.h files !")

    file_names_list = cmd_output.splitlines()
    for file_name in file_names_list:

        file_content = get_file_content(file_name)
        if file_content != '':
            print('INF parsing - %s' % (file_name))

            content_lines = file_content.splitlines()
            for data_line in content_lines:

                m = re.match(r"^#define\s*(__NR_[\w]*)[ \t]+(\d+)$", data_line)
                if m:
                    name = m.group(1)
                    num = m.group(2)
                    # remove prefix '__NR_'
                    names.append({'name': name[5:], 'num': num})
    return names

def read_file(csv_file):
    with open(csv_file) as csvfile:
        reader = csv.DictReader(csvfile)

        res = {}
        for row in reader:

            name = ''
            if row['name'].startswith("sys_"):
                name = row['name'][4:]
            else:
                name = row['name']

            # count number of arguments, exclude 'name' and 'num' field
            args = -2
            if 'argc' in row:
                args = int(row['argc'])
            else:
                for i in row:
                    if row[i] != '':
                        args += 1

            obj = {
                'args': args,
                'name': name,
                'argv': [row['first'], row['second'], row['third'], row['fourth'], row['fifth'], row['sixth']]
            }
            res[name] = obj

        return res


def get_ret_arg(syscall_name):
    cmd_output = get_cmd_output('man 2 %s 2>/dev/null' % (syscall_name))
    if cmd_output == '':
        # manual no for this syscall not exist
        return None

    # find SYNOPSIS block
    fs = r'(.*\n)*SYNOPSIS((.*\n)*)DESCRIPTION'
    m = re.match(fs, cmd_output)
    if not m:
        return None

    cmd_output = m.group(2)

    fs = r'(.*\n)*\s+((\w+[*]?\s+[*]?){1,2})%s[(]' % (syscall_name)
    m = re.match(fs, cmd_output)
    if m:
        return m.group(2)

    # print("Can't find syscall prototype in manual for %s !" % (syscall_name))
    return None

# def get_syscall_from_man(syscall_name):
#     cmd_output = get_cmd_output('man 2 %s 2>/dev/null' % (syscall_name))
#     if cmd_output == '':
#         # manual no for this syscall not exist
#         return None

#     # find SYNOPSIS block
#     fs = r'(.*\n)*SYNOPSIS((.*\n)*)DESCRIPTION'
#     m = re.match(fs, cmd_output)
#     if not m:
#         print("block not found")
#         return None

#     cmd_output = m.group(2)

#     fs = r'(.*\n)*\s+((\w+[*]?\s+[*]?){1,2})%s[(](([\n]?\s*\w*\s*\w*\s*[*]?\w*\[?\]?\s*[,]?([.]{3})?[^,)]*)*)[)][;]' % (syscall_name)

#     m = re.match(fs, cmd_output)
#     if m:
#         ret_val = m.group(2)
#         argv = m.group(4)
#         argv = ' '.join(argv.splitlines())
#         argv = ' '.join(argv.split())

#         return {'ret': ret_val, 'argv': argv}

#     print("Can't find [ %s ] !" % (syscall_name))
#     return None


def main(args):
    if not args:
        print >> sys.stderr, "Usage: %s [path to kernel dir]" % (sys.argv[0],)
        return 1

    csv_file = args[0]
    src_file_f = 'get_syscall_inf_{}.c'
    syscall_types = read_file(csv_file)

    arch = '64'
    syscall_names = get_syscalls_names(arch)
    write_file(src_file_f.format(arch), arch, syscall_types, syscall_names)

    arch = '32'
    syscall_names = get_syscalls_names(arch)
    write_file(src_file_f.format(arch), arch, syscall_types, syscall_names)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))