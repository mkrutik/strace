# Python 2.7.16
import sys
import re
import os

def get_cmd_output(cmd):
    cmd_out = ''
    cmd_out = os.popen(cmd).read()
    return cmd_out

def get_file_content(file_name):
    content = ''
    if os.path.exists(file_name):
        fp = open(file_name, "r")
        content = fp.read()
        fp.close()
    return content

# return list of syscall_info objects
def get_syscalls_names(arch, syscall_names):
    cmd_output = get_cmd_output('locate -r "/usr/include/.*/asm/unistd_%s.h$"' % (arch))
    if cmd_output == '':
        sys.exit("Can't find any unistd_64.h files !")

    file_names_list = cmd_output.splitlines()
    for file_name in file_names_list:

        file_content = get_file_content(file_name)
        if file_content != '':
            print('INF parsing - %s' % (file_name))

            content_lines = file_content.splitlines()
            for data_line in content_lines:

                m = re.match(r"^#define\s*(__NR_[\w]*)[ \t]+(\d+)$", data_line)
                if m:
                    name = m.group(1)
                    # num = m.group(2)

                    # remove prefix '__NR_'
                    syscall_names.add(name[5:])


def get_syscall_inf_from_man(syscall_name):
    cmd_output = get_cmd_output('man 2 %s 2>/dev/null' % (syscall_name))
    if cmd_output == '':
        # manual no for this syscall not exist
        return None

    if syscall_name == 'keyctl':
        return None

    # find SYNOPSIS block
    fs = r'(.*\n)*SYNOPSIS((.*\n)*)DESCRIPTION'
    m = re.match(fs, cmd_output)
    if not m:
        print("block not found")
        return None

    cmd_output = m.group(2)

    if re.search(r'%s([(][^)]*(([.]{3})|(/[*]))[^)]*[)])' % (syscall_name), cmd_output) :
        return None

    fs = r'(.*\n)*\s+((\w+[*]?\s+[*]?){1,2})%s[(](([\n]?\s*\w*\s*\w*\s*[*]?\w*\[?\]?\s*[,]?)*)[)]' % (syscall_name)
    m = re.match(fs, cmd_output)
    if m:
        ret_val = m.group(2)
        argv = m.group(4)
        argv = ' '.join(argv.splitlines())
        argv = ' '.join(argv.split())

        return {'ret': ret_val, 'argv': argv}

    return None


def main(args):
    syscall_names = set()

    get_syscalls_names('32', syscall_names)
    get_syscalls_names('64', syscall_names)

    out = open("syscall_list.csv", 'w')
    print('"name","ret","argc","first","second","third","fourth","fifth","sixth"', file=out)

    for elem in sorted(syscall_names):
        out.write('"%s",' % (elem))

        inf = get_syscall_inf_from_man(elem)
        if inf == None:
            out.write('"None","","","","","","",""')
        else:
            out.write('"%s",' % (inf['ret']))

            argv = inf['argv'].split(',')
            out.write('"%d",' % (len(argv)))
            
            for arg in argv:
                out.write('"%s",' % (arg))

            i = len(argv)
            while i < 6:
                out.write('"",')
                i += 1

        out.write('\n')

    out.close()

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))