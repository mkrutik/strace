# Python 2.7.16
import csv
import sys
import re
import os

def detect_type(ctype):
    if  re.search(r'((const)?\s*(char){1}\s*[*]{1})\w*$', ctype) or \
        re.search(r'((const)?\s*(char){1}\s*[*]{1}\s*(const){1}\s*)\w*$', ctype) or \
        re.search(r'((const)?\s*(char){1}\s*(const)?\s*[*]{1}[\s\t]*)\w*$', ctype) or \
        re.search(r'caddr_t', ctype) :

        return "ARG_STR"

    if  re.search(r'[*]+', ctype) or \
        re.search(r'struct', ctype) or \
        re.search(r'cap_user_data_t', ctype) or \
        re.search(r'cap_user_header_t', ctype) or \
        re.search(r'sighandler_t', ctype) or \
        re.search(r'union', ctype) :

        return "ARG_ADDR"

    if  re.search(r'(unsigned)\s*(long)\s*(long)', ctype) or \
        re.search(r'u64', ctype) or \
        re.search(r'uint64_t', ctype) or \
        re.search(r'size_t', ctype) :
        return "ARG_LL_UINT"

    if  re.search(r'(unsigned)\s*(long)', ctype) or \
        re.search(r'u32', ctype) or \
        re.search(r'aio_context_t', ctype) or \
        re.search(r'nfds_t', ctype) or \
        re.search(r'dev_t', ctype) or \
        re.search(r'old_sigset_t', ctype):

        return "ARG_L_UINT"

    if  re.search(r'unsigned', ctype) or \
        re.search(r'uid_t', ctype) or \
        re.search(r'gid_t', ctype) or \
        re.search(r'mode_t', ctype) or \
        re.search(r'umode_t', ctype) or \
        re.search(r'qid_t', ctype) or \
        re.search(r'socklen_t', ctype) or \
        re.search(r'enum', ctype) or \
        re.search(r'id_t', ctype) or \
        re.search(r'idtype_t', ctype) or \
        re.search(r'uint', ctype) :

        return "ARG_UINT"

    if  re.search(r'(long)\s*(long)', ctype) or \
        re.search(r'ssize_t', ctype) :
        return "ARG_LL_INT"

    if  re.search(r'long', ctype) or \
        re.search(r'loff_t', ctype) or \
        re.search(r'timer_t', ctype) or \
        re.search(r'clock_t', ctype) or \
        re.search(r'off64_t', ctype) or \
        re.search(r'time_t', ctype) or \
        re.search(r'off_t', ctype) :
        
        return "ARG_L_INT"

    if  re.search(r'int', ctype) or \
        re.search(r'__s32', ctype) or \
        re.search(r'pid_t', ctype) or \
        re.search(r'key_t', ctype) or \
        re.search(r'clockid_t', ctype) or \
        re.search(r'mqd_t', ctype) or \
        re.search(r'key_serial_t', ctype) or \
        re.search(r'int', ctype) :

        return "ARG_INT"

    if re.search(r'void', ctype) :
        return 'ARG_NONE'

    return 'None'


def write_file(src_file, arch, syscall_types, syscall_names):
        out = open(src_file, 'w')

        print('#include "ft_syscall.h"\n', file=out)
        print('const t_syscall_inf* get_syscall_%s(int num)' % (arch), file=out)
        print('{', file=out)
        print('\tstatic t_syscall_inf syscalls[] = {', file=out)

        unique_arg = set()

        for elem in syscall_names:
            name = elem['name']
            num = elem['num']
            syscall_inf = {}

            name_in_list = False
            for index in sorted(syscall_types):
                if name == syscall_types[index]['name']:
                    syscall_inf = syscall_types[index]
                    name_in_list = True
                    break

            if not name_in_list:
                print('// Unimplemented system call. "%s"' % (name), file=out)
                continue

            argc = syscall_inf['argc']
            argv = syscall_inf['argv']
            ret_type = detect_type(syscall_inf['ret'])

            if syscall_inf['ret'] == "NOT_IMPLEMENTED":
                out.write("// %s  - NOT_IMPLEMENTED\n" % (name))
                continue

            print(argc , name)
            argc = int(argc)
            out.write("\t\t[%+3s]  { %-26s %-15s %d, " % (num, "\"" + name + "\",", ret_type + ",", argc,))

            if argc > 0:
                out.write('{')
                for arg in argv:
                    if arg != None and arg != '' and arg != 'NOT IMPLEMENTED':
                        out.write("%s, " % (detect_type(arg)))
                        arg = re.sub(r'(\s?[\w]+)$', '', arg)

                        if not re.search(r'[*]+', arg):
                            unique_arg.add(arg)

                    elif arg == 'NOT IMPLEMENTED':
                        out.write('/* NOT IMPLEMENTED !!! */')

                out.write('}')
            else:
                out.write("{}")
            print('},', file=out)

        print('\t};', file=out)
        print('\tstatic const int LIST_SIZE = sizeof(syscalls) / sizeof(t_syscall_inf);\n', file=out)
        print('\tconst t_syscall_inf *res = NULL;', file=out)
        print('\tif (num >= 0 && num < LIST_SIZE)', file=out)
        print('\t\tres = &(syscalls[num]);', file=out)
        print('\treturn res;', file=out)
        print('}', file=out)
        out.close()

        # write all data types to file for testing purpose
        out = open('args_{}.h'.format(arch), 'w')
        for arg in sorted(unique_arg):
            print(arg, file=out)
        out.close()

def get_cmd_output(cmd):
    cmd_out = ''
    cmd_out = os.popen(cmd).read()
    return cmd_out

def get_file_content(file_name):
    content = ''
    if os.path.exists(file_name):
        fp = open(file_name, "r")
        content = fp.read()
        fp.close()
    return content

# return list of syscall_info objects
def get_syscalls_names(arch):
    names = []

    cmd_output = get_cmd_output('locate -r "/usr/include/.*/asm/unistd_%s.h$"' % (arch))
    if cmd_output == '':
        sys.exit("Can't find any unistd_64.h files !")

    file_names_list = cmd_output.splitlines()
    for file_name in file_names_list:

        file_content = get_file_content(file_name)
        if file_content != '':
            print('INF parsing - %s' % (file_name))

            content_lines = file_content.splitlines()
            for data_line in content_lines:

                m = re.match(r"^#define\s*(__NR_[\w]*)[ \t]+(\d+)$", data_line)
                if m:
                    name = m.group(1)
                    num = m.group(2)
                    # remove prefix '__NR_'
                    names.append({'name': name[5:], 'num': num})
    return names

def read_file(csv_file):
    with open(csv_file) as csvfile:
        reader = csv.DictReader(csvfile)

        res = {}
        for row in reader:
            name = row['name']
            #  "name","ret","argc","first","second","third","fourth","fifth","sixth"
            obj = {
                'name': name,
                'ret': row['ret'],
                'argc': row['argc'],
                'argv': [row['first'], row['second'], row['third'], row['fourth'], row['fifth'], row['sixth']]
            }
            res[name] = obj

        return res

def main(args):
    if not args:
        print >> sys.stderr, "Usage: %s [path to kernel dir]" % (sys.argv[0],)
        return 1

    csv_file = args[0]
    src_file_f = 'get_syscall_inf_{}.c'
    syscall_types = read_file(csv_file)

    arch = '64'
    syscall_names = get_syscalls_names(arch)
    write_file(src_file_f.format(arch), arch, syscall_types, syscall_names)

    arch = '32'
    syscall_names = get_syscalls_names(arch)
    write_file(src_file_f.format(arch), arch, syscall_types, syscall_names)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))