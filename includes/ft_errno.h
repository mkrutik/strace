/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_errno.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/15 16:28:31 by mkrutik           #+#    #+#             */
/*   Updated: 2019/10/21 22:37:39 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ERRNO_H
#define FT_ERRNO_H

#include <errno.h>
#include <stddef.h>

typedef struct  s_errno
{
    const int   num;
    const char  *name;
    const char  *desc;
}               t_errno;

typedef enum    e_errno_flag
{
    ERRN_NAME,
    ERRN_DESC
}               t_errno_flag;

const char      *get_errno_inf(int num, t_errno_flag flag);

#endif