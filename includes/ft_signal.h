/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/15 13:50:51 by mkrutik           #+#    #+#             */
/*   Updated: 2019/10/21 22:35:14 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SIGNAL_H
#define FT_SIGNAL_H

#include <signal.h>
#include <stddef.h>

typedef struct  s_signal
{
    const int   num;
    const char  *name;
}               t_signal;

const char      *get_signal_name(int sig_num);

#endif