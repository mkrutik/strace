/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arguments.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/15 17:30:18 by mkrutik           #+#    #+#             */
/*   Updated: 2019/10/21 22:37:55 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ARGUMENTS_H
#define FT_ARGUMENTS_H

#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

extern int optind;
extern int opterr;

int        get_arguments(int argc, char **argv, bool *out);

#endif