/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_syscall.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/15 21:51:38 by mkrutik           #+#    #+#             */
/*   Updated: 2019/10/21 22:37:28 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SYSCALL_H
#define FT_SYSCALL_H

#include <syscall.h>
#include <string.h>
#include <stdint.h>

typedef enum            e_arg_type
{
    ARG_STR,
    ARG_ADDR,
    ARG_INT,
    ARG_L_INT,
    ARG_LL_INT,
    ARG_UINT,
    ARG_L_UINT,
    ARG_LL_UINT,
    ARG_NONE
}                       t_arg_type;

typedef struct          s_syscall_inf
{
    const char          *name;

    const t_arg_type    ret;

    const uint8_t       n_args;
    const t_arg_type    args[6];
}                       t_syscall_inf;

const t_syscall_inf     *get_syscall_64(int num);
const t_syscall_inf     *get_syscall_32(int num);

#endif