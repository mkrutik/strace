/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strace.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/18 21:20:46 by mkrutik           #+#    #+#             */
/*   Updated: 2019/10/23 23:03:21 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRACE_H
# define FT_STRACE_H

#include <elf.h>
#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <sys/stat.h>

#include <wait.h>
#include <stdio.h>
#include <errno.h>
#include <stddef.h>

#include <ctype.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>

#include "ft_arguments.h"
#include "ft_errno.h"
#include "ft_signal.h"
#include "ft_syscall.h"


#ifdef COLOR
#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_YELLOW  "\x1b[33m"
#define COLOR_BLUE    "\x1b[36m" //"\x1b[34m"
#define COLOR_RESET   "\x1b[0m"
#else
#define COLOR_RED     ""
#define COLOR_GREEN   ""
#define COLOR_YELLOW  ""
#define COLOR_BLUE    ""
#define COLOR_RESET   ""
#endif

extern pid_t				g_child;
extern int					g_status;

typedef struct  			s_32_user_regs_struct {
    uint32_t    			ebx;
    uint32_t    			ecx;
    uint32_t    			edx;
    uint32_t    			esi;
    uint32_t    			edi;
    uint32_t    			ebp;
    uint32_t    			eax;
    uint32_t    			xds;
    uint32_t    			xes;
    uint32_t    			xfs;
    uint32_t    			xgs;
    uint32_t    			orig_eax;
    uint32_t    			eip;
    uint32_t    			xcs;
    uint32_t    			eflags;
    uint32_t    			esp;
    uint32_t    			xss;
}               			t_x32_user_regs_struct;

typedef union               u_reg_inf
{
    t_x32_user_regs_struct  reg_32;
    struct user_regs_struct reg_64;
}                           t_reg_inf;

typedef struct      		s_iovec {
    void            		*iov_base;
    unsigned int    		iov_len;

    const t_syscall_inf     *sys_inf;
    int                     print_len;
    bool                    is_x64;
}                   		t_iovec;


void   						tracer(void);

int    						tracee(const char* file_path,
                                   int argc,
                                   char *argv[]);

int                         wait_for_syscall(void);
int                         attache_to_child(void);


int							check_file(char file_path[FILENAME_MAX],
                                       const char *name);

size_t						print_x64_args(const t_syscall_inf *inf,
                                           struct user_regs_struct *regs);

size_t						print_x32_args(const t_syscall_inf *inf,
                                           const t_x32_user_regs_struct *regs);

void					    read_string(char buff[50],
                                        const unsigned long long int addr);

void						print_ret(const unsigned long long int reg,
                                        t_arg_type type,
                                        size_t print_len,
                                        bool is_x64);

#endif